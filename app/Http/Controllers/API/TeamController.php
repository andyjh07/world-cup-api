<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index()
    {
        return response()->json([
            [
                "name" => "Argentina",
                "flag" => '🇦🇷',
                "owner" => "Emily"
            ],
            [
                "name" => "Australia",
                "flag" => "🇦🇺",
                "owner" => "Miranda"
            ],
            [
                "name" => "Belgium",
                "flag" => "🇧🇪",
                "owner" => "Coralie"
            ],
            [
                "name" => "Colombia",
                "flag" => "🇨🇴",
                "owner" => "Julie"
            ],
            [
                "name" => "Costa Rica",
                "flag" => "🇨🇷",
                "owner" => "Kelly"
            ],
            [
                "name" => "Croatia",
                "flag" => "🇭🇷",
                "owner" => "Emily"
            ],
            [
                "name" => "Denmark",
                "flag" => "🇩🇰",
                "owner" => "Mark"
            ],
            [
                "name" => "Egypt",
                "flag" => "🇪🇬",
                "owner" => "Mike"
            ],
            [
                "name" => "England",
                "flag" => "🏴󠁧󠁢󠁥󠁮󠁧󠁿",
                "owner" => "Sam"
            ],
            [
                "name" => "France",
                "flag" => "🇫🇷",
                "owner" => "Andy"
            ],
            [
                "name" => "Germany",
                "flag" => "🇩🇪",
                "owner" => "Andy"
            ],
            [
                "name" => "Iceland",
                "flag" => "🇮🇸",
                "owner" => "Laura"
            ],
            [
                "name" => "Iran",
                "flag" => "🇮🇷",
                "owner" => "Rebecca"
            ],
            [
                "name" => "Japan",
                "flag" => "🇯🇵",
                "owner" => "Diane"
            ],
            [
                "name" => "Mexico",
                "flag" => "🇲🇽",
                "owner" => "Roger"
            ],
        ]);
    }

    public function teamFlag(string $country)
    {

    }
}
