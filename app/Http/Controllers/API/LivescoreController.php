<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LivescoreController extends Controller
{
    /**
     * Return the current live World Cup Game
     */
    public function index()
    {
        $apiKey = env('API_KEY');
        $apiSecret = env('API_SECRET');
        
        $liveMatch = [];
        $worldCupGroups = [
            793, 794, 795, 796, 797, 798, 799, 800
        ];

        $json = file_get_contents("http://livescore-api.com/api-client/scores/live.json?key={$apiKey}&secret={$apiSecret}");
        $decoded = json_decode($json);

        foreach($decoded->data->match as $match){
            // if(in$match->league_id)
            if(in_array($match->league_id, $worldCupGroups) && $match->status != "FINISHED"){
                array_push($liveMatch, $match);
            }
        }

        return json_encode($liveMatch);
    }

    /**
     * Return the flags for the current World Cup game
     */
    public function flags(string $home, string $away)
    {
        dd($home.$away);
    }
}
