@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
    <div id="liveGame"></div>
    <div class="row">
        <div class="col-md-12">
            <br>
            <h2>Today's Matches</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="todaysMatches">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Time</th>
                            <th scope="col">Teams</th>
                            <th scope="col">Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="match in todaysMatches.fixtures">
                            <td>@{{ match.date | moment('timezone', 'Europe/London', 'ha') }}</td>
                            <td>
                                <img :src="match.homeFlag" style="height: 32px;">
                                @{{ match.homeTeamName }} vs @{{ match.awayTeamName }} 
                                <img :src="match.awayFlag" style="height: 32px;">
                            </td>
                            <td>@{{ match.result.goalsHomeTeam || 0 }} - @{{ match.result.goalsAwayTeam || 0 }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection