
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

window.axios.defaults.headers.common = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
};

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

// import Vue from 'vue'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VueTruncate from 'vue-truncate-filter'

Vue.use(VueTruncate)
Vue.use(VueMoment, {
    moment,
});

var config = {
    headers: {'X-Auth-Token': '718acdaae4ce43c8abb276b10233e8f2'}
};

const todaysMatches = new Vue({
    el: '#todaysMatches',
    data: {
        todaysMatches: []
    },
    methods: {
        loadData: async function() {
            const response = await axios.get(
              "http://api.football-data.org/v1/competitions/467/fixtures/?timeFrame=n1",
              config
            );
          
            this.todaysMatches = response.data;
            const fixtures = this.todaysMatches.fixtures;
          
            let arr = fixtures.map(fixture => {
              const _links = fixture._links;
              return [
                axios.get(_links.awayTeam.href, config),
                axios.get(_links.homeTeam.href, config)
              ];
            });
          
            arr.forEach(async (item, index) => {
              const away = await item[0];
              const home = await item[1];
              this.$set(fixtures, index, {
                ...fixtures[index],
                awayFlag: away.data.crestUrl,
                homeFlag: home.data.crestUrl
              });
            });
          }
    },
    mounted: function () {
        this.loadData();
    }
});

var LiveGame = require('./components/LiveGame.vue');
var liveGame = new Vue({
    el: '#liveGame',
    render: function (createElement) {
        return createElement(LiveGame);
    }
});